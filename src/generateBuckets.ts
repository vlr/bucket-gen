import { ContentFile } from "@vlr/gulp-transform";
import { groupBy } from "@vlr/map-tools";
import { parseTypesInFiles } from "@vlr/type-parser";
import { GeneratorOptions } from "./common/generatorOptions.type";
import { generateBucket } from "./generateBucket";

const defaultOptions: GeneratorOptions = {
  quotes: "\"",
  linefeed: "\n",
  aliasMap: {}
};

export function generateBuckets(files: ContentFile[], options: GeneratorOptions): ContentFile[] {
  options = {
    ...defaultOptions,
    ...options || {}
  };
  const filtered = files.filter(x => x.extension === ".ts");
  const parsed = parseTypesInFiles(filtered, options);
  const grouped = groupBy(parsed, file => file.file.folder);
  const buckets = grouped.values()
    .map(grp => generateBucket(grp, options))
    .filter(x => x != null);
  return buckets;
}


