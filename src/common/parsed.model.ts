import { TypesModel } from "@vlr/type-parser";
import { ContentFile } from "@vlr/gulp-transform";

export type ParsedModel = TypesModel<ContentFile>;
