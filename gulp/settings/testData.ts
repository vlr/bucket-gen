import { settings } from "./settings";

export function testDataFolder(): string {
  return "./" + settings.testData;
}

export function testDataSrcFolder(): string {
  return "./" + settings.testDataSrc;
}
