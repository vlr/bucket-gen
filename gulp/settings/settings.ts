export const settings = {
  src: "src",
  testSrc: "testSrc",
  testData: "testData",
  testDataSrc: "testDataSrc",
  build: "./build",
};
