
import { projectName } from "./projectName";
import { getGitPrivateKey, getNpmToken } from "./homeFiles";
import { request } from "./request";

let projectId = 11456152;

export async function createRepoOnGitLab(): Promise<void> {
  const body = {
    name: await projectName(),
    issues_enabled: true,
    merge_requests_enabled: true,
    jobs_enabled: true,
    visibility: "public",
    only_allow_merge_if_pipeline_succeeds: true,
    only_allow_merge_if_all_discussions_are_resolved: true,
    merge_method: "ff"
  };
  const result = await request.post("/projects", body);
  projectId = result.id;
}

export async function setRepoEnvVariables(): Promise<void> {
  await setVar("GIT_PRIVATE_KEY", await getGitPrivateKey());
  await setVar("NPM_AUTH_TOKEN", await getNpmToken());
}

async function setVar(key: string, value: string): Promise<void> {
  const body = {
    id: projectId,
    key,
    value,
    protected: true
  };
  await request.post(`/projects/${projectId}/variables`, body);
}

export async function setMasterProtection(): Promise<void> {
  await request.del(`/projects/${projectId}/protected_branches/master`);
  const body = {
    id: projectId,
    name: "master",
    merge_access_level: 40,
    push_access_level: 0
  };
  await request.post(`/projects/${projectId}/protected_branches`, body);
}
