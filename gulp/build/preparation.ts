import { parallel } from "gulp";
import { removeBuildFolder, tslint } from "./parts";

export const preparation = parallel(removeBuildFolder, tslint);
