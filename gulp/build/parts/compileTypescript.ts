import { spawn } from "@vlr/spawn";

export function compileTypescript(): Promise<void> {
  return spawn("compileTypescript", "npx", "tsc");
}
