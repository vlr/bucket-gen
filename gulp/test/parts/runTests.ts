import * as gulp from "gulp";
import { settings, testJsFiles } from "../../settings";
import * as mocha from "gulp-mocha";

function executeTests(folder: string): NodeJS.ReadWriteStream {
  return gulp.src(testJsFiles(folder))
    .pipe(mocha({ reporter: "spec" }));
}

export function runTests(): NodeJS.ReadWriteStream {
  return executeTests(settings.build);
}
