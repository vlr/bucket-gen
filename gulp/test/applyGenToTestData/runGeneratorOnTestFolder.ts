import { ContentFile, transformRange } from "@vlr/gulp-transform";
import * as gulp from "gulp";
import { runGenerator } from "./runGenerator";

export function runGeneratorOnTestFolder(source: string, target: string, folder: string): NodeJS.ReadWriteStream {
  return gulp.src(source + "/" + folder + "/**/*.ts")
    .pipe(transformRange(concatSourceFilesAndGenerated))
    .pipe(gulp.dest(target + "/" + folder));
}

function concatSourceFilesAndGenerated(files: ContentFile[]): ContentFile[] {
  const generated = runGenerator(files);
  return [...files, ...generated];
}
